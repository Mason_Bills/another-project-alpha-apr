from django.shortcuts import render, redirect, get_object_or_404
from tasks.forms import TaskForm
from tasks.models import Task
from projects.models import Project
from django.contrib.auth.decorators import login_required
from django.views.generic.edit import DeleteView


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save(False)
            task.owner = request.user
            task.save()
            return redirect("show_my_tasks")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def task_list(request):
    task_list = Task.objects.filter(assignee=request.user)
    context = {
        "task_list": task_list,
    }
    return render(request, "tasks/Tlist.html", context)


def edit_task(request, id):
    task = Task.objects.get(id=id)
    if request.method == "POST":
        form = TaskForm(request.POST, instance=task)
        if form.is_valid():
            form.save(False)
            # was form.save()

            return redirect("show_my_tasks")
    else:
        form = TaskForm(instance=task)
        # instance=project

    context = {"form": form}

    return render(request, "tasks/edit.html", context)


@login_required
# add notes feature!!
def show_task(request, id):
    show_task = get_object_or_404(Task, id=id)
    show_project = show_task.project
    context = {"show_task": show_task, "show_project": show_project}
    return render(request, "tasks/Tdetail.html", context)


# "show_project": show_project


def delete_task(request, id):
    task = Task.objects.get(id=id)
    # was .filter(id=id)

    if request.method == "POST":
        task.delete()
        return redirect("show_my_task")

    context = {"task": task}
    return render(request, "tasks/Tdelete.html", context)
