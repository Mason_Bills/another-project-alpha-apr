from django.urls import path
from tasks.views import (
    create_task,
    task_list,
    edit_task,
    show_task,
    delete_task,
)

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", task_list, name="show_my_tasks"),
    path("mine/edit/<int:id>", edit_task, name="edit_task"),
    path("mine/show/<int:id>", show_task, name="show_task"),
    path("mine/delete/<int:id>", delete_task, name="delete_task"),
]
