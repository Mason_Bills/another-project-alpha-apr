from django.contrib import admin

# Register your models here.
from tasks.models import Task


# Register your models here.
@admin.register(Task)
class ProjectAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "start_date",
        "due_date",
        "is_completed",
        "assignee",
    )
