from django.db import models
from django.conf import settings


# Create your models here.
class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateField(null=True)
    due_date = models.DateField(null=True)
    is_completed = models.BooleanField(
        default=False,
    )
    notes = models.TextField(
        null=True,
        blank=True,
    )
    assignee = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        related_name="tasks",
        on_delete=models.CASCADE,
    )

    project = models.ForeignKey(
        "projects.Project",
        related_name="tasks",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.name
