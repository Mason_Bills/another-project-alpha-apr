from django.urls import path
from projects.views import (
    project_list,
    show_project,
    create_project,
    delete_project,
    update_project,
    search_projects,
)


urlpatterns = [
    path("", project_list, name="list_projects"),
    path("<int:id>/", show_project, name="show_project"),
    path("create/", create_project, name="create_project"),
    path("<int:id>/delete/", delete_project, name="delete_project"),
    path("<int:id>/edit/", update_project, name="update_project"),
    path("search/", search_projects, name="search"),
]
# name="list_projects" defines the variable that will be used in the html file.
