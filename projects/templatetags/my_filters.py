from django import template
from tasks.models import Task
from projects.models import Project

register = template.Library()


@register.filter
def is_project(obj):
    return isinstance(obj, Project)


register.filter("is_project", is_project)


@register.filter
def is_task(obj):
    return isinstance(obj, Task)


register.filter("is_task", is_task)
