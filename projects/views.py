from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from tasks.models import Task
from projects.forms import ProjectForm
import re


@login_required
def project_list(request):
    list_projects = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": list_projects,
    }
    return render(request, "projects/list.html", context)


# "list_projects" is the varibale that is being used in the html files


@login_required
def show_project(request, id):
    show_project = get_object_or_404(Project, id=id)
    context = {
        "show_project": show_project,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.purchaser = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)


def update_project(request, id):
    project = Project.objects.get(id=id)
    if request.method == "POST":
        form = ProjectForm(request.POST, instance=project)
        if form.is_valid():
            form.save()

            return redirect("list_projects")
    else:
        form = ProjectForm(instance=project)
        # "instance=project" is telling django which model attributes to use(?)

    context = {"form": form}

    return render(request, "projects/edit.html", context)


# funct was delete_queryset
def delete_project(request, id):
    project = Project.objects.get(id=id)
    # was .filter(id=id)

    if request.method == "POST":
        project.delete()
        return redirect("list_projects")

    context = {"project": project}
    return render(request, "projects/delete.html", context)


@login_required
def search_projects(request):
    search_query = request.GET.get(
        "q", ""
    )  # Get the search query from the request, default to an empty string if not provided
    pattern = re.compile(
        search_query, re.IGNORECASE
    )  # Create a case-insensitive regex pattern

    # Get all objects from your model
    all_projects = Project.objects.filter(owner=request.user)
    all_tasks = Task.objects.filter(assignee=request.user)

    # Filter objects based on whether their fields match the pattern
    matching_projects = [
        obj for obj in all_projects if pattern.search(obj.name)
    ]
    matching_task = [obj for obj in all_tasks if pattern.search(obj.name)]

    matching_objects = matching_projects + matching_task

    context = {
        "search_query": search_query,
        "matching_objects": matching_objects,
    }

    return render(request, "projects/search.html", context)
